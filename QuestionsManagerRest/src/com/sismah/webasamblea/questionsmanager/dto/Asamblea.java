package com.sismah.webasamblea.questionsmanager.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Asamblea {
	
	private String codAsamblea;
	private String nombreAsamblea;
	
	public String getCodAsamblea() {
		return codAsamblea;
	}
	public void setCodAsamblea(String codAsamblea) {
		this.codAsamblea = codAsamblea;
	}
	public String getNombreAsamblea() {
		return nombreAsamblea;
	}
	public void setNombreAsamblea(String nombreAsamblea) {
		this.nombreAsamblea = nombreAsamblea;
	}
	
}
