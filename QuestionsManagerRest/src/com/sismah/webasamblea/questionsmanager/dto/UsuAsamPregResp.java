package com.sismah.webasamblea.questionsmanager.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UsuAsamPregResp {
	
	private String codUsuario;
	private String codAsamblea;
	private String codPregunta;
	private String codRespuesta;
	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodAsamblea() {
		return codAsamblea;
	}
	public void setCodAsamblea(String codAsamblea) {
		this.codAsamblea = codAsamblea;
	}
	public String getCodPregunta() {
		return codPregunta;
	}
	public void setCodPregunta(String codPregunta) {
		this.codPregunta = codPregunta;
	}
	public String getCodRespuesta() {
		return codRespuesta;
	}
	public void setCodRespuesta(String codRespuesta) {
		this.codRespuesta = codRespuesta;
	}
	
}
