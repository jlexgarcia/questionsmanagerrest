package com.sismah.webasamblea.questionsmanager.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pregunta {
	
	private String codPregunta;
	private String nombrePregunta;
	private String tipoPregunta;
	private List<Opcion> opciones;
	
	public String getCodPregunta() {
		return codPregunta;
	}
	public void setCodPregunta(String codPregunta) {
		this.codPregunta = codPregunta;
	}
	public String getNombrePregunta() {
		return nombrePregunta;
	}
	public void setNombrePregunta(String nombrePregunta) {
		this.nombrePregunta = nombrePregunta;
	}
	public String getTipoPregunta() {
		return tipoPregunta;
	}
	public void setTipoPregunta(String tipoPregunta) {
		this.tipoPregunta = tipoPregunta;
	}
	public List<Opcion> getOpciones() {
		return opciones;
	}
	public void setOpciones(List<Opcion> opciones) {
		this.opciones = opciones;
	}
	
}
