package com.sismah.webasamblea.questionsmanager.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Opcion {
	
	private String codOpcion;
	private String nombreOpcion;
	
	public String getCodOpcion() {
		return codOpcion;
	}
	public void setCodOpcion(String codOpcion) {
		this.codOpcion = codOpcion;
	}
	public String getNombreOpcion() {
		return nombreOpcion;
	}
	public void setNombreOpcion(String nombreOpcion) {
		this.nombreOpcion = nombreOpcion;
	}
	
}
