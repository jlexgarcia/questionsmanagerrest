package com.sismah.webasamblea.questionsmanager.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Respuesta {
	
	private int codigoRespuesta;
	private String mensajeRespuesta;
	
	public static final int SUCCESS_CODE = 0;
	public static final int ERROR_CODE = 1;
	
	public Respuesta() {
		super();
	}
	public Respuesta(int codigoRespuesta, String mensajeRespuesta) {
		super();
		this.codigoRespuesta = codigoRespuesta;
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	public int getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(int codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
}
