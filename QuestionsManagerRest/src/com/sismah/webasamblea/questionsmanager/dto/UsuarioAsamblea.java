package com.sismah.webasamblea.questionsmanager.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UsuarioAsamblea {
	
	private String codUsuario;
	private String codAsamblea;
	
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodAsamblea() {
		return codAsamblea;
	}
	public void setCodAsamblea(String codAsamblea) {
		this.codAsamblea = codAsamblea;
	}
	
}
