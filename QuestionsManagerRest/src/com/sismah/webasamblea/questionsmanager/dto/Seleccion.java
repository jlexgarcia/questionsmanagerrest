package com.sismah.webasamblea.questionsmanager.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Seleccion {
	
	private String codCompany;
	private String codGroup;
	private String codUsuario;
	private String nombreUsuario;
	private String codPregunta;
	private String codOpcion;
	
	public String getCodCompany() {
		return codCompany;
	}
	public void setCodCompany(String codCompany) {
		this.codCompany = codCompany;
	}
	public String getCodGroup() {
		return codGroup;
	}
	public void setCodGroup(String codGroup) {
		this.codGroup = codGroup;
	}
	public String getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getCodPregunta() {
		return codPregunta;
	}
	public void setCodPregunta(String codPregunta) {
		this.codPregunta = codPregunta;
	}
	public String getCodOpcion() {
		return codOpcion;
	}
	public void setCodOpcion(String codOpcion) {
		this.codOpcion = codOpcion;
	}
	
}
