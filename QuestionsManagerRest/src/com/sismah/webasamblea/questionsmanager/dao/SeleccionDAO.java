package com.sismah.webasamblea.questionsmanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Properties;

import com.sismah.webasamblea.questionsmanager.dto.Respuesta;
import com.sismah.webasamblea.questionsmanager.util.ConexionUtil;
import com.sismah.webasamblea.questionsmanager.util.PropiedadesUtil;

public class SeleccionDAO {
	
	public Respuesta responderPregunta(String codCompany, String codGroup, String codUsuario, String nombreUsuario, String codPregunta, String codOpcion) {
		PropiedadesUtil propiedadesUtil = new PropiedadesUtil();
		Properties msj = propiedadesUtil.cargarArchivoPropiedades();
		Respuesta respuesta = new Respuesta();
		boolean nuevaSeleccion = true;
		
		Connection con = null;
		PreparedStatement pst = null;
		
		try {
			con = ConexionUtil.getConexion();
			String sql = msj.getProperty("registra.nueva.seleccion");
			pst = con.prepareStatement(sql);
			pst.setString(1, codCompany);
			pst.setString(2, codGroup);
			pst.setString(3, codUsuario);
			pst.setString(4, nombreUsuario);
			pst.setString(5, codPregunta);
			pst.setString(6, codOpcion);
			pst.execute();
		} catch (Exception e) {
			nuevaSeleccion = false;
			e.printStackTrace();
		} finally {
			ConexionUtil.close(con, pst, null);
		}
		if (nuevaSeleccion) {
			respuesta.setCodigoRespuesta(Respuesta.SUCCESS_CODE);
			respuesta.setMensajeRespuesta("Se ha registrado su selecci�n");
		} else {
			respuesta.setCodigoRespuesta(Respuesta.ERROR_CODE);
			respuesta.setMensajeRespuesta("Se ha presentado un inconveniente al registrar su selecci�n");
		}
		
		return respuesta;
	}
	
}
