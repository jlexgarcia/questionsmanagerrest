package com.sismah.webasamblea.questionsmanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.sismah.webasamblea.questionsmanager.dto.Opcion;
import com.sismah.webasamblea.questionsmanager.util.ConexionUtil;
import com.sismah.webasamblea.questionsmanager.util.PropiedadesUtil;

public class OpcionDAO {
	
	public List<Opcion> consultarOpciones(String codUSuario, String codPregunta) {
		PropiedadesUtil propiedadesUtil = new PropiedadesUtil();
		Properties msj = propiedadesUtil.cargarArchivoPropiedades();
		List<Opcion> opciones = new ArrayList<Opcion>();
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			con = ConexionUtil.getConexion();
			String sql = msj.getProperty("consulta.lista.opciones");
			pst = con.prepareStatement(sql);
			pst.setString(1, codPregunta);
			rs = pst.executeQuery();
			while (rs.next()) {
				Opcion opcion = new Opcion();
				opcion.setCodOpcion(rs.getString("responseOptionId"));
				opcion.setNombreOpcion(rs.getString("description"));
				opciones.add(opcion);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConexionUtil.close(con, pst, rs);
		}
		
		return opciones;
	}
	
}
