package com.sismah.webasamblea.questionsmanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.sismah.webasamblea.questionsmanager.dto.Pregunta;
import com.sismah.webasamblea.questionsmanager.util.ConexionUtil;
import com.sismah.webasamblea.questionsmanager.util.PropiedadesUtil;

public class PreguntaDAO {
	
	public List<Pregunta> consultarPreguntas(String codUSuario, String codAsamblea) {
		PropiedadesUtil propiedadesUtil = new PropiedadesUtil();
		Properties msj = propiedadesUtil.cargarArchivoPropiedades();
		List<Pregunta> preguntas = new ArrayList<Pregunta>();
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			con = ConexionUtil.getConexion();
			String sql = msj.getProperty("consulta.lista.preguntas");
			pst = con.prepareStatement(sql);
			pst.setString(1, codAsamblea);
			rs = pst.executeQuery();
			while (rs.next()) {
				Pregunta pregunta = new Pregunta();
				pregunta.setCodPregunta(rs.getString("questionId"));
				pregunta.setNombrePregunta(rs.getString("question_resolution"));
				pregunta.setTipoPregunta(rs.getString("question_type"));
				OpcionDAO opcionDAO = new OpcionDAO();
				pregunta.setOpciones(opcionDAO.consultarOpciones(codUSuario, pregunta.getCodPregunta()));
				preguntas.add(pregunta);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConexionUtil.close(con, pst, rs);
		}
		
		return preguntas;
	}
	
	
	public Pregunta consultarPreguntaActual(String codUSuario, String codAsamblea) {
		PropiedadesUtil propiedadesUtil = new PropiedadesUtil();
		Properties msj = propiedadesUtil.cargarArchivoPropiedades();
		Pregunta pregunta = new Pregunta();
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			con = ConexionUtil.getConexion();
			String sql = msj.getProperty("consulta.pregunta.actual");
			pst = con.prepareStatement(sql);
			pst.setString(1, codAsamblea);
			rs = pst.executeQuery();
			
			while (rs.next()) {
				pregunta.setCodPregunta(rs.getString("questionId"));
				pregunta.setNombrePregunta(rs.getString("question_resolution"));
				pregunta.setTipoPregunta(rs.getString("question_type"));
				OpcionDAO opcionDAO = new OpcionDAO();
				pregunta.setOpciones(opcionDAO.consultarOpciones(codUSuario, pregunta.getCodPregunta()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConexionUtil.close(con, pst, rs);
		}
		
		return pregunta;
	}
	
}
