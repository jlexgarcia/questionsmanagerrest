package com.sismah.webasamblea.questionsmanager.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.sismah.webasamblea.questionsmanager.dto.Asamblea;
import com.sismah.webasamblea.questionsmanager.util.ConexionUtil;
import com.sismah.webasamblea.questionsmanager.util.PropiedadesUtil;

public class AsambleaDAO {
		
	public List<Asamblea> consultarAsambleas(String codUsuario) {
		PropiedadesUtil propiedadesUtil = new PropiedadesUtil();
		Properties msj = propiedadesUtil.cargarArchivoPropiedades();
		List<Asamblea> asambleas = new ArrayList<Asamblea>();
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			con = ConexionUtil.getConexion();
			String sql = msj.getProperty("consulta.lista.asambleas");
			pst = con.prepareStatement(sql);
			pst.setString(1, codUsuario);
			rs = pst.executeQuery();
			while (rs.next()) {
				Asamblea asamblea = new Asamblea();
				asamblea.setCodAsamblea(rs.getString("assemblyId"));
				asamblea.setNombreAsamblea(rs.getString("name"));
				asambleas.add(asamblea);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConexionUtil.close(con, pst, rs);
		}
		
		return asambleas;
	}
	
}
