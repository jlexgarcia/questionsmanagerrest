package com.sismah.webasamblea.questionsmanager.rest;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.sismah.webasamblea.questionsmanager.dao.AsambleaDAO;
import com.sismah.webasamblea.questionsmanager.dao.PreguntaDAO;
import com.sismah.webasamblea.questionsmanager.dao.SeleccionDAO;
import com.sismah.webasamblea.questionsmanager.dto.Asamblea;
import com.sismah.webasamblea.questionsmanager.dto.Pregunta;
import com.sismah.webasamblea.questionsmanager.dto.Respuesta;
import com.sismah.webasamblea.questionsmanager.dto.Seleccion;
import com.sismah.webasamblea.questionsmanager.dto.Usuario;
import com.sismah.webasamblea.questionsmanager.dto.UsuarioAsamblea;

@Path("/asamblea_manager")
public class ConsultaAsambleaManager {
	
	@POST
	@Consumes("*/*")
	@Produces("application/json; charset=utf-8")
	@Path("consultarAsambleas")
	public String consultarAsambleas(InputStream codUsuario) {
		List<Asamblea> asambleas = new ArrayList<Asamblea>();
		StringBuilder codUsuarioBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(codUsuario));
			String line = null;
			while ((line = in.readLine()) != null) {
				codUsuarioBuilder.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		Usuario usuarioObject = new Usuario();
		String jsonString = codUsuarioBuilder.toString();
		InputStream is = new ByteArrayInputStream(jsonString.getBytes());
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		usuarioObject = gson.fromJson(br, Usuario.class);
		AsambleaDAO asambleaDAO = new AsambleaDAO();
		asambleas = asambleaDAO.consultarAsambleas(usuarioObject.getCodUsuario());
		String response = gson.toJson(asambleas);
		return response;
	}
	
	
	@POST
	@Consumes("*/*")
	@Produces("application/json; charset=utf-8")
	@Path("consultarPreguntas")
	public String consultarPreguntas(InputStream usuarioAsamblea) {
		List<Pregunta> preguntas = new ArrayList<Pregunta>();
		StringBuilder usuAsamBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(usuarioAsamblea));
			String line = null;
			while ((line = in.readLine()) != null) {
				usuAsamBuilder.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		UsuarioAsamblea usuAsamObject = new UsuarioAsamblea();
		String jsonString = usuAsamBuilder.toString();
		InputStream is = new ByteArrayInputStream(jsonString.getBytes());
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		usuAsamObject = gson.fromJson(br, UsuarioAsamblea.class);
		PreguntaDAO preguntaDAO = new PreguntaDAO();
		preguntas = preguntaDAO.consultarPreguntas(usuAsamObject.getCodUsuario(), usuAsamObject.getCodAsamblea());
		String response = gson.toJson(preguntas);
		return response;
	}
	
	
	@POST
	@Consumes("*/*")
	@Produces("application/json; charset=utf-8")
	@Path("consultarPreguntaActual")
	public String consultarPreguntaActual(InputStream usuarioAsamblea) {
		Pregunta pregunta = new Pregunta();
		StringBuilder usuAsamBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(usuarioAsamblea));
			String line = null;
			while ((line = in.readLine()) != null) {
				usuAsamBuilder.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		UsuarioAsamblea usuAsamObject = new UsuarioAsamblea();
		String jsonString = usuAsamBuilder.toString();
		InputStream is = new ByteArrayInputStream(jsonString.getBytes());
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		usuAsamObject = gson.fromJson(br, UsuarioAsamblea.class);
		PreguntaDAO preguntaDAO = new PreguntaDAO();
		pregunta = preguntaDAO.consultarPreguntaActual(usuAsamObject.getCodUsuario(), usuAsamObject.getCodAsamblea());
		String response = gson.toJson(pregunta);
		return response;
	}
	
	
	@POST
	@Consumes("*/*")
	@Produces("application/json; charset=utf-8")
	@Path("responderPregunta")
	public String responderPregunta(InputStream seleccion) {
		Respuesta respuesta = new Respuesta();
		StringBuilder seleccionBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(seleccion));
			String line = null;
			while ((line = in.readLine()) != null) {
				seleccionBuilder.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Gson gson = new Gson();
		Seleccion seleccionObject = new Seleccion();
		String jsonString = seleccionBuilder.toString();
		InputStream is = new ByteArrayInputStream(jsonString.getBytes());
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		seleccionObject = gson.fromJson(br, Seleccion.class);
		SeleccionDAO seleccionDAO = new SeleccionDAO();
		respuesta = seleccionDAO.responderPregunta(seleccionObject.getCodCompany(), seleccionObject.getCodGroup(), seleccionObject.getCodUsuario(), seleccionObject.getNombreUsuario(), seleccionObject.getCodPregunta(), seleccionObject.getCodOpcion());
		String response = gson.toJson(respuesta);
		return response;
	}
	
}
