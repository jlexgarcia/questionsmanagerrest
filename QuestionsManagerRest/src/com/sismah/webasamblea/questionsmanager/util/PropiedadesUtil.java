package com.sismah.webasamblea.questionsmanager.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropiedadesUtil {
	
	Properties msj = new Properties();
	private String msjFileName = "messages.properties";
	private InputStream inputStream = getClass().getClassLoader().getResourceAsStream(msjFileName);
	
	public Properties cargarArchivoPropiedades () {
		try {
			if (msj.isEmpty()){
				msj.load(inputStream);	
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return msj;
	}
		
}
