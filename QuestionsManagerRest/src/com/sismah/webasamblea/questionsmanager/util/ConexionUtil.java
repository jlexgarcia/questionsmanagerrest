package com.sismah.webasamblea.questionsmanager.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//import javax.naming.Context;
//import javax.naming.InitialContext;
//import javax.naming.NamingException;
//import javax.servlet.annotation.WebServlet;
//import javax.sql.DataSource;

public class ConexionUtil {
	
	public static Connection getConexion() {
//		Context ctx = null;
//		Connection con = null;
//		try {
//			ctx = new InitialContext();
//			DataSource ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/LocalAssemblyDB");
//			con = ds.getConnection();
//		} catch(NamingException e) {
//			e.printStackTrace();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			String url = "jdbc:mysql://localhost/lportal?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			String user = "root";
			String pass = "root";
			
			con = DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return con;
	}
	
	 public static void close(Connection conn, Statement stmt, ResultSet rs) {
	      try {
	    	  if (conn != null) {
	              conn.close();
	          }
	          if (stmt != null) {
	        	  stmt.close();
	          }
	          if (rs != null) {
	              rs.close();
	          }
	      } catch (SQLException e) {
	      	e.printStackTrace();
	      }
	  }
	
}
